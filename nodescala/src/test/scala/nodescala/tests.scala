package nodescala

import nodescala.NodeScala._
import org.junit.runner.RunWith
import org.scalatest._
import org.scalatest.concurrent.AsyncAssertions
import org.scalatest.junit.JUnitRunner

import scala.collection._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Success, Failure}

@RunWith(classOf[JUnitRunner])
class NodeScalaSuite extends FunSuite with ShouldMatchers with AsyncAssertions {

  test("A Future should always be completed") {
    val always = Future.always(517)
    assert(Await.result(always, 0 nanos) == 517)
  }

  test("A Future should never be completed") {
    val never = Future.never[Int]
    try {
      Await.result(never, 5 second)
      fail()
    } catch {
      case t: TimeoutException => println("ok")
    }
  }

  test("A Future.any should be completed with 517") {
    val any = Future.any(List(Future.never, Future.always(517), Future.never))
    assert(Await.result(any, 1 seconds) === 517)
  }

  test("A Future.any should be completed with 517 (real futures)") {
    val any = Future.any(List(Future {
      blocking {
        Thread.sleep(1000)
      }
    }, Future.always(517), Future {
      blocking {
        Thread.sleep(5000)
      }
    }))
    assert(Await.result(any, 1 seconds) === 517)
  }

  test("A Future.any should be completed with exception (real futures)") {
    val any = Future.any(List(Future {
      blocking {
        Thread.sleep(1000)
      }
    }, Future {
      throw new IllegalStateException()
    }, Future {
      blocking {
        Thread.sleep(5000)
      }
    }))
    try {
      Await.result(any, 5 second)
    } catch {
      case e: IllegalStateException => println("ok")
      case _: Exception => fail()
    }
  }

  test("A Future.all should be completed with list of (1,2,3)") {
    val all = Future.all(List(Future(1), Future(2), Future(3)))
    assert(Await.result(all, 1 seconds) === List(1, 2, 3))
  }

  test("A delayed Future.all should be completed with list of (1,2,3)") {
    val all = Future.all(List(Future {
      blocking {
        Thread.sleep(3000)
        1
      }
    }, Future(2), Future {
      blocking {
        Thread.sleep(1000)
        3
      }
    }))
    assert(Await.result(all, 5 seconds) === List(1, 2, 3))
  }

  test("A Future should be completed after 1s delay") {
    val w = new Waiter
    val start = System.currentTimeMillis()

    Future.delay(1 second) onComplete { case _ =>
      val duration = System.currentTimeMillis() - start
      duration should (be >= 1000L and be < 1100L)

      w.dismiss()
    }

    w.await(timeout(2 seconds))
  }

  test("Two sequential delays of 1s should delay by 2s") {
    val w = new Waiter
    val start = System.currentTimeMillis()

    val combined = for {
      f1 <- Future.delay(1 second)
      f2 <- Future.delay(1 second)
    } yield ()

    combined onComplete { case _ =>
      val duration = System.currentTimeMillis() - start
      duration should (be >= 2000L and be < 2100L)

      w.dismiss()
    }

    w.await(timeout(3 seconds))
  }

  test("Future should be completed with 142 now") {
    val now = Future(142).now
    assert(now === 142)
  }

  test("Future should be completed with NoSuchElementException now") {
    try {
      val now = Future({
        blocking {
          Thread.sleep(1000)
        }
      }).now
    } catch {
      case _: NoSuchElementException => println("ok")
      case _: Exception => fail()
    }
  }

  test("Future 142 should be completed with NoSuchElementException now") {
    try {
      val now = Future({
        blocking {
          Thread.sleep(1)
          142
        }
      }).now
    } catch {
      case _: NoSuchElementException => println("ok")
      case _: Exception => fail()
    }
  }

  test("continueWith should wait for the first future to complete") {
    val w = new Waiter
    val start = System.currentTimeMillis()
    val cont = (future: Future[Unit]) => 42
    val continued: Future[Int] = Future.delay(1 second).continueWith(cont)
    continued onComplete {
      case _ =>
        val duration = System.currentTimeMillis() - start
        duration should (be >= 1000L and be < 1100L)
        w.dismiss()
    }
    w.await(timeout(2 seconds))
  }

  test("continueWith should be completed with 42") {
    val cont = (future: Future[Unit]) => 42
    val continued: Future[Int] = Future.delay(1 second).continueWith(cont)
    assert(Await.result(continued, 2 seconds) === 42)
  }

  test("continueWith should not be completed with 42 after exception") {
    val cont = (future: Future[Unit]) => 42
    val continued: Future[Int] = Future(throw new IllegalStateException).continueWith(cont)
    assert(Await.result(continued, 2 seconds) === 42)
  }

  class DummyExchange(val request: Request) extends Exchange {
    @volatile var response = ""
    val loaded = Promise[String]()

    def write(s: String) {
      response += s
    }

    def close() {
      loaded.success(response)
    }
  }

  class DummyListener(val port: Int, val relativePath: String) extends NodeScala.Listener {
    self =>

    @volatile private var started = false
    var handler: Exchange => Unit = null

    def createContext(h: Exchange => Unit) = this.synchronized {
      assert(started, "is server started?")
      handler = h
    }

    def removeContext() = this.synchronized {
      assert(started, "is server started?")
      handler = null
    }

    def start() = self.synchronized {
      started = true
      new Subscription {
        def unsubscribe() = self.synchronized {
          started = false
        }
      }
    }

    def emit(req: Request) = {
      val exchange = new DummyExchange(req)
      if (handler != null) handler(exchange)
      exchange
    }
  }

  class DummyServer(val port: Int) extends NodeScala {
    self =>
    val listeners = mutable.Map[String, DummyListener]()

    def createListener(relativePath: String) = {
      val l = new DummyListener(port, relativePath)
      listeners(relativePath) = l
      l
    }

    def emit(relativePath: String, req: Request) = this.synchronized {
      val l = listeners(relativePath)
      l.emit(req)
    }
  }

  test("Server should serve requests") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => for (kv <- request.iterator) yield (kv + "\n").toString
    }

    // wait until server is really installed
    Thread.sleep(500)

    def test(req: Request) {
      val webpage = dummy.emit("/testDir", req)
      val content = Await.result(webpage.loaded.future, 1 second)
      val expected = (for (kv <- req.iterator) yield (kv + "\n").toString).mkString
      assert(content == expected, s"'$content' vs. '$expected'")
    }

    test(immutable.Map("StrangeRequest" -> List("Does it work?")))
    test(immutable.Map("StrangeRequest" -> List("It works!")))
    test(immutable.Map("WorksForThree" -> List("Always works. Trust me.")))

    dummySubscription.unsubscribe()
  }

}




