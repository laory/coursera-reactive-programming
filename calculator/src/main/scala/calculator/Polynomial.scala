package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
                   c: Signal[Double]): Signal[Double] = {
    Signal {
      val bVal: Double = b()
      val aVal: Double = a()
      val cVal: Double = c()
      bVal * bVal - 4 * aVal * cVal
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
                       c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      val bVal: Double = b()
      val aVal: Double = a()
      val dVal: Double = delta()
      dVal match {
        case d if d < 0.0 => Set()
        case 0.0 => Set(computeRoot(aVal, bVal, 0))
        case d =>
          val dSqrt: Double = Math sqrt d
          Set(computeRoot(aVal, bVal, dSqrt), computeRoot(aVal, bVal, -dSqrt))
      }
    }
  }

  private def computeRoot(a: Double, b: Double, dSqrt: Double): Double = {
    (-b + dSqrt) / (2 * a)
  }
}
