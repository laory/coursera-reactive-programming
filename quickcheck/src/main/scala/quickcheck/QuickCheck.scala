package quickcheck

import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import org.scalacheck.Prop._
import org.scalacheck._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: A =>
    val h = insert(a, empty)
    ord.equiv(findMin(h), a)
  }

  property("gen1") = forAll { (h: H, a: A) =>
    val m = if (isEmpty(h)) a else findMin(h)
    ord.equiv(findMin(insert(m, h)), m)
  }

  property("hint1 - If you insert any two elements into an empty heap, finding the minimum of the resulting heap " +
    "should get the smallest of the two elements back") = forAll { (a: A, b: A) =>
    val min = if (ord.lteq(a, b)) a else b
    ord.equiv(findMin(insert(a, insert(b, empty))), min)
  }

  property("hint2 - If you insert an element into an empty heap, then delete the minimum, the resulting heap should " +
    "be empty") = forAll { (a: A) =>
    deleteMin(insert(a, empty)) == empty
  }

  property("hint3 - Given any heap, you should get a sorted sequence of elements when continually finding and deleting " +
    "minimal") = forAll { (h: H) =>
    isSorted(heap2List(h))
  }

  property("hint4 - Finding a minimum of the melding of any two heaps should return a minimum of one or the other") =
    forAll { (h1: H, h2: H) =>
      val minInMelded: A = findMin(meld(h1, h2))
      ord.equiv(findMin(h1), minInMelded) || ord.equiv(findMin(h2), minInMelded)
    }

  property("Given any two heaps, you should get a sorted sequence of elements when continually finding and deleting " +
    "minimal") = forAll { (h1: H, h2: H) =>
    isSorted(heap2List(meld(h1, h2)))
  }

  property("Given any two lists, you should get the same sorted lists of elements when melding heaps made from them or continually " +
    "adding them to heap") = forAll { (list1: List[A], list2: List[A]) =>
    val heap: H = list2Heap(list1 ::: list2)
    val meldedHeap: H = meld(list2Heap(list1), list2Heap(list2))
    val heap2List1: List[A] = heap2List(heap)
    val heap2List2: List[A] = heap2List(meldedHeap)
    isSorted(heap2List1) && isSorted(heap2List2) && heap2List2 == heap2List1
  }

  lazy val genHeap: Gen[H] = for {
    a <- arbitrary[A]
    h <- frequency((3, const(empty)), (7, genHeap))
  } yield insert(a, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  def heap2List(h: H): List[A] = {
    def heap2ListIntern(collector: List[A], heap: H): List[A] = {
      if (isEmpty(heap)) collector else heap2ListIntern(collector :+ findMin(heap), deleteMin(heap))
    }
    heap2ListIntern(List(), h)
  }

  def list2Heap(list: List[A]): H = {
    def list2HeapIntern(collector: H, list: List[A]): H = {
      if (list.isEmpty) collector else list2HeapIntern(insert(list.head, collector), list.tail)
    }
    list2HeapIntern(empty, list)
  }

  def isSorted(list: List[A]): Boolean = {
    isSorted(list, ord)
  }

  def isSorted(list: List[A], ordering: Ordering[A]): Boolean = list match {
    case List() => true
    case List(_) => true
    case head :: tail => ordering.lteq(head, tail.head) && isSorted(tail, ordering)
  }

}
