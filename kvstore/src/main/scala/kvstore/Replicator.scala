package kvstore

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object Replicator {

  case class Replicate(key: String, valueOption: Option[String], id: Long)

  case class Replicated(key: String, id: Long)

  case class Snapshot(key: String, valueOption: Option[String], seq: Long)

  case class SnapshotAck(key: String, seq: Long)

  case object Retry

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor with ActorLogging {

  import context.dispatcher
  import kvstore.Replicator._

  import scala.concurrent.duration._

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]

  val retryScheduler = context.system.scheduler.schedule(0.millis,
    100.millis,
    self,
    Retry)

  var _seqCounter = 0L

  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  def receive: Receive = handleReplicate orElse handleSnapshotAck orElse handleRetry

  def handleReplicate(): Receive = {
    case op: Replicate =>
      val senderRef = sender()
      val seq = nextSeq
      acks += (seq ->(senderRef, op))
      log.info("replicator replicate {}", pending)
  }

  def handleSnapshotAck(): Receive = {
    case op: SnapshotAck =>
      log.info("replicator snapshotAck {}", pending)
      acks.get(op.seq) match {
        case Some((sender, message)) =>
          sender ! Replicated(message.key, message.id)
          acks -= op.seq
        case None =>
      }
  }

  def handleRetry(): Receive = {
    case Retry =>
      log.info(s"before retry: $pending")
      pending = Vector.empty[Snapshot]
      acks foreach { case (seq, (_, operation: Replicate)) =>
        val snapshot: Snapshot = Snapshot(operation.key, operation.valueOption, seq)
        if (!pending.contains(snapshot)) pending :+= snapshot
      }
      pending foreach { task =>
        replica ! task
      }
      log.info(s"after retry: $pending")
  }

}
