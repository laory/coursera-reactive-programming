package kvstore

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor._
import kvstore.Arbiter._
import kvstore.Persistence.{Persist, Persisted, PersistenceException}
import kvstore.Replicator.{Replicate, Replicated, Snapshot, SnapshotAck}

import scala.concurrent.forkjoin.ThreadLocalRandom

object Replica {

  sealed trait Operation {
    def key: String

    def id: Long
  }

  case class Insert(key: String, value: String, id: Long) extends Operation

  case class Remove(key: String, id: Long) extends Operation

  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply

  case class OperationAck(id: Long) extends OperationReply

  case class OperationFailed(id: Long) extends OperationReply

  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  case object Ping

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor with ActorLogging {

  import context.dispatcher
  import kvstore.Replica._

  import scala.concurrent.duration._

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */
  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()
    arbiter ! Join
  }

  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  var seq = 0L

  val persistence = context.actorOf(persistenceProps)
  var opLog = Map.empty[Long, (ActorRef, Long, Option[Persist], Set[ActorRef])]

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 5, withinTimeRange = 1.second) {
      case _: PersistenceException => Restart
      case _: Exception => Stop
    }

  val pingInterval = 100.millis
  val retryTimeout = 1.second
  val pingScheduler = context.system.scheduler.schedule(
    initialDelay = pingInterval,
    pingInterval,
    receiver = self,
    message = Ping
  )

  def receive = {
    case JoinedPrimary => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  val leader: Receive = handleInsert orElse handleRemove orElse handleGet orElse handlePersisted(primary = true) orElse
    handlePing(asPrimary = true) orElse handleReplicas orElse handleReplicated

  val replica: Receive = handleGet orElse handleSnapshot orElse handlePersisted(primary = false) orElse
    handlePing(asPrimary = false)

  private def handleInsert(): Receive = {
    case op: Insert =>
      kv += (op.key -> op.value)
      persist(op.key, Some(op.value), op.id)
      replicate(op.key, Some(op.value), op.id)
      log.info("primary insert - {}", kv)
  }

  private def handleRemove(): Receive = {
    case op: Remove =>
      kv -= op.key
      persist(op.key, None, op.id)
      replicate(op.key, None, op.id)
      log.info("primary remove - {}", kv)
  }

  private def handleGet(): Receive = {
    case op: Get =>
      sender() ! GetResult(op.key, kv.get(op.key), op.id)
  }

  private def handleSnapshot(): Receive = {
    case op: Snapshot =>
      if (op.seq < seq) sender() ! SnapshotAck(op.key, op.seq)
      else if (op.seq == seq) {
        op.valueOption match {
          case Some(value) => kv += (op.key -> value)
          case None => kv -= op.key
        }
        persist(op.key, op.valueOption, op.seq)
        log.info("secondary snapshot - {}", opLog)
      }
  }

  private def handlePersisted(primary: Boolean): Receive = {
    case msg: Persisted =>
      logPersistenceFinished(msg, primary)    
      log.info("{} persisted - {}", if (primary) "primary" else "secondary", opLog)
  }

  private def handleReplicated(): Receive = {
    case msg: Replicated =>
      var replicatorRef = sender()
      logReplicationFinished(msg, replicatorRef)
      log.info("primary replicated - {}", opLog)
  }

  private def handlePing(asPrimary: Boolean): Receive = {
    case Ping =>
      checkLiveOps(asPrimary)
      cleanupOutdatedOps(asPrimary)
      log.info("{} retry - {}", if (asPrimary) "primary" else "secondary", opLog)
  }

  private def checkLiveOps(primary: Boolean) = {
    opLog filter {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        timeElapsed(loggedInitTime) <= retryTimeout.toMillis
    } foreach {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        if (loggedPersist.isDefined) persistence ! loggedPersist.get
        else if (primary && loggedReplicators.isEmpty) {
          opLog -= loggedId
          loggedInitiator ! OperationAck(loggedId)
        }
    }
  }

  private def cleanupOutdatedOps(primary: Boolean) = {
    opLog filter {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        timeElapsed(loggedInitTime) > retryTimeout.toMillis
    } foreach {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        opLog -= loggedId
        if (primary) loggedInitiator ! OperationFailed(loggedId)
    }
  }

  private def handleReplicas(): Receive = {
    case msg: Replicas =>
      val replicas = msg.replicas - self
      cleanupBrokenReplicas(replicas)
      initReplicationForNewReplicas(replicas)
      replicateAllValues()
      log.info("primary replicas - {}", opLog)
  }

  private def cleanupBrokenReplicas(replicasUpdate: Set[ActorRef]) = {
    secondaries filterNot {
      case (secondary, replicator) =>
        replicasUpdate.contains(secondary)
    } foreach {
      case (secondary, replicator) =>
        secondaries -= secondary
        replicators -= replicator
        decreaseReplicationsCount(replicator)
        context.stop(replicator)
    }
  }

  private def initReplicationForNewReplicas(replicasUpdate: Set[ActorRef]) = {
    replicasUpdate foreach { replica =>
      if (!secondaries.contains(replica)) {
        val replicator: ActorRef = context.actorOf(Replicator.props(replica))
        secondaries += (replica -> replicator)
        replicators += replicator
      }
    }
  }

  private def replicateAllValues() = {
    kv foreach {
      case (key, value) =>
        val id = nextSeq
        replicate(key, Some(value), id)
    }
  }

  private def replicate(key: String, valueOption: Option[String], id: Long) = {
    val initiator = sender()
    replicators foreach { replicator =>
      logReplication(id, initiator, replicator)
      replicator ! Replicate(key, valueOption, id)
    }
  }

  private def persist(key: String, valueOption: Option[String], id: Long) = {
    val persistMessage = Persist(key, valueOption, id)
    val initiator = sender()
    persistence ! persistMessage
    logPersistence(id, initiator, persistMessage)
  }

  private def timeElapsed(startTime: Long) = {
    System.currentTimeMillis() - startTime
  }

  private def logReplication(id: Long, initiator: ActorRef, replicator: ActorRef) = {
    opLog find {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        loggedId == id && initiator == loggedInitiator
    } match {
      case Some((loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators))) =>
        opLog += (id ->(loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators + replicator))
      case None =>
        opLog += (id ->(initiator, System.currentTimeMillis(), None, Set(replicator)))
    }
  }

  private def logPersistence(id: Long, initiator: ActorRef, msg: Persist) = {
    opLog find {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        loggedId == id && initiator == loggedInitiator
    } match {
      case Some((loggedId, (loggedInitiator, loggedInitTime, None, loggedReplicators))) =>
        opLog += (id ->(loggedInitiator, loggedInitTime, Some(msg), loggedReplicators))
      case None =>
        opLog += (id ->(initiator, System.currentTimeMillis(), Some(msg), Set.empty[ActorRef]))
      case _ =>
    }
  }

  private def logPersistenceFinished(msg: Persisted, primary: Boolean) = {
    opLog find {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        loggedId == msg.id && loggedPersist.isDefined && loggedPersist.get.key == msg.key
    } match {
      case Some((loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators))) =>
        if (timeElapsed(loggedInitTime) <= retryTimeout.toMillis) {
          if (!primary) loggedInitiator ! SnapshotAck(msg.key, msg.id)
          seq += 1
        }
        opLog += (loggedId ->(loggedInitiator, loggedInitTime, None, loggedReplicators))
      case None =>
    }
  }

  private def logReplicationFinished(msg: Replicated, replicator: ActorRef) = {
    opLog find {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        loggedId == msg.id
    } match {
      case Some((loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators))) =>
        opLog += (loggedId ->(loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators - replicator))
      case None =>
    }
  }

  private def decreaseReplicationsCount(deadReplicator: ActorRef) = {
    opLog filter {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        timeElapsed(loggedInitTime) <= retryTimeout.toMillis
    } foreach {
      case (loggedId, (loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators)) =>
        opLog += (loggedId ->(loggedInitiator, loggedInitTime, loggedPersist, loggedReplicators - deadReplicator))
    }
  }

  private def nextSeq = {
    ThreadLocalRandom.current().nextLong()
  }
}

