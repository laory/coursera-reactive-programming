/**
 * Copyright (C) 2013-2015 Typesafe Inc. <http://www.typesafe.com>
 */
package kvstore

import akka.actor.{ Actor, Props, ActorRef, ActorSystem }
import akka.testkit.{ TestProbe, ImplicitSender, TestKit }
import kvstore.Persistence.{Persist, Persisted}
import org.scalatest.{ BeforeAndAfterAll, FlatSpec, Matchers }
import scala.concurrent.duration._
import org.scalatest.FunSuiteLike
import org.scalactic.ConversionCheckedTripleEquals

class IntegrationSpec(_system: ActorSystem) extends TestKit(_system)
    with FunSuiteLike
        with Matchers
    with BeforeAndAfterAll
    with ConversionCheckedTripleEquals
    with ImplicitSender
    with Tools {

  import Replica._
  import Replicator._
  import Arbiter._

  def this() = this(ActorSystem("ReplicatorSpec"))

  override def afterAll: Unit = system.shutdown()

  /*
   * Recommendation: write a test case that verifies proper function of the whole system,
   * then run that with flaky Persistence and/or unreliable communication (injected by
   * using an Arbiter variant that introduces randomly message-dropping forwarder Actors).
   */

  test("case1: everything should work by specs") {
    val arbiter = TestProbe()
    val persistence = TestProbe()
    val primary = system.actorOf(Replica.props(arbiter.ref, probeProps(persistence)), "case1-primary")
    val user = session(primary)

    arbiter.expectMsg(Join)
    arbiter.send(primary, JoinedPrimary)

    val secondary = TestProbe()
    secondary.send(arbiter.ref, Join)
    arbiter.expectMsg(Join)
    arbiter.send(secondary.ref, JoinedSecondary)
    secondary.expectMsg(JoinedSecondary)

    arbiter.send(primary, Replicas(Set(primary, secondary.ref)))

    val ack1 = user.set("k1", "v1")
    persistence.expectMsg(200.millis, Persist("k1", Some("v1"), 0L))
    persistence.expectMsg(200.millis, Persist("k1", Some("v1"), 0L))
    persistence.send(primary, Persisted("k1", 0L))
    secondary.expectMsg(200.millis, Snapshot("k1", Some("v1"), 0L))
    secondary.expectMsg(Snapshot("k1", Some("v1"), 0L))
    secondary.reply(SnapshotAck("k1", 0L))
    user.waitAck(ack1)

    val ack2 = user.set("k1", "v2")
    secondary.expectMsg(Snapshot("k1", Some("v2"), 1L))
    arbiter.send(primary, Replicas(Set(primary)))
    persistence.send(primary, Persisted("k1", 1L))
    user.waitAck(ack2)
  }

  test("case2: everything should work by specs with flaky persistence") {
    val arbiter = TestProbe()
    val primary = system.actorOf(Replica.props(arbiter.ref, Persistence.props(flaky = true)), "case2-primary")
    val user = session(primary)

    arbiter.expectMsg(Join)
    arbiter.send(primary, JoinedPrimary)

    val secondary = TestProbe()
    secondary.send(arbiter.ref, Join)
    arbiter.expectMsg(Join)
    arbiter.send(secondary.ref, JoinedSecondary)
    secondary.expectMsg(JoinedSecondary)

    arbiter.send(primary, Replicas(Set(primary, secondary.ref)))

    for (i <- 1 to 10) yield {
      val ack = user.set(s"k$i", s"v$i")
      secondary.expectMsg(1.seconds, Snapshot(s"k$i", Some(s"v$i"), i - 1))
      secondary.reply(SnapshotAck(s"k$i", i - 1))
      user.waitAck(ack)
    }

    val ack11 = user.set("k11", "v11")
    secondary.expectMsg(1.seconds, Snapshot("k11", Some("v11"), 10L))
    arbiter.send(primary, Replicas(Set(primary)))
    user.waitAck(ack11)
  }
}
