/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import actorbintree.BinaryTreeNode.{CopyFinished, CopyTo}
import akka.actor._

import scala.collection.immutable.Queue
import scala.util.Random

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef

    def id: Int

    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection */
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {

  import actorbintree.BinaryTreeSet._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case operation: Operation =>
      root ! operation
    case GC =>
      val newRoot = createRoot
      context.become(garbageCollecting(newRoot))
      root ! CopyTo(newRoot)
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case operation: Operation =>
      pendingQueue = pendingQueue.enqueue(operation)
    case CopyFinished =>
      context.stop(root)
      root = newRoot
      pendingQueue = dequeue(pendingQueue, operation => root ! operation)
      context.unbecome()
  }

  private def dequeue(queue: Queue[Operation], f: Operation => Unit): Queue[Operation] = {
    if (queue.isEmpty) queue
    else {
      queue.dequeue match {
        case (operation, lessQueue) =>
          f(operation)
          dequeue(lessQueue, f)
      }
    }
  }

}

object BinaryTreeNode {

  trait Position

  case object Left extends Position

  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)

  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {

  import actorbintree.BinaryTreeNode._
  import actorbintree.BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = handleContains orElse handleInsert orElse handleRemove orElse handleCopyTo

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case OperationFinished(_) =>
      if (expected.isEmpty) {
        context.parent ! CopyFinished
      } else context.become(copying(expected, insertConfirmed = true), discardOld = true)
    case CopyFinished =>
      val updatedExpected = expected - sender()
      if (updatedExpected.isEmpty && insertConfirmed) {
        context.parent ! CopyFinished
      } else context.become(copying(updatedExpected, insertConfirmed), discardOld = true)
  }

  private def handleContains: Receive = {
    case msg: Contains =>
      def checkChild(position: Position) = {
        if (subtrees.contains(position)) subtrees(position) ! msg
        else msg.requester ! ContainsResult(msg.id, result = false)
      }
      if (msg.elem == elem) msg.requester ! ContainsResult(msg.id, result = !removed)
      else if (msg.elem < elem) checkChild(Left)
      else checkChild(Right)
  }

  private def handleInsert: Receive = {
    case msg: Insert =>
      def insertInChild(position: Position) = {
        if (subtrees.contains(position)) subtrees(position) ! msg
        else {
          subtrees = subtrees + (position -> context.actorOf(props(msg.elem, initiallyRemoved = false)))
          msg.requester ! OperationFinished(msg.id)
        }
      }
      if (msg.elem == elem) {
        if (removed) removed = false
        msg.requester ! OperationFinished(msg.id)
      } else if (msg.elem < elem) insertInChild(Left)
      else insertInChild(Right)
  }

  private def handleRemove: Receive = {
    case msg: Remove =>
      def removeFromChild(position: Position) = {
        if (subtrees.contains(position)) subtrees(position) ! msg
        else msg.requester ! OperationFinished(msg.id)
      }
      if (msg.elem == elem) {
        removed = true
        msg.requester ! OperationFinished(msg.id)
      } else if (msg.elem < elem) removeFromChild(Left)
      else removeFromChild(Right)
  }

  private def handleCopyTo: Receive = {
    case msg: CopyTo =>
      context.become(copying(subtrees.values.toSet, insertConfirmed = false))
      subtrees.values foreach {leaf => leaf ! msg}
      if (!removed) {
        msg.treeNode ! Insert(self, 0, elem)
      } else {
        self ! OperationFinished(0)
      }
  }
}
